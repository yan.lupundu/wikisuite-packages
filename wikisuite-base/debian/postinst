#!/bin/bash

DEFAULT_PHP=8.1

MYSQL_SERVICE=mysql

MIN_VALUE_FOR_MYSQL_MAX_ALLOWED_PACKET_SIZE="64M"

# Helper functions (extensions to slib)
run() {
  echo "$2"
  eval "$1"
}

run_file_exists() {
  if [ ! -f "$3" ] || [ ! -d "$3" ] ; then
    return 0
  fi
  run "$1" "$2"
}

configvalue() {
  local sc_config="$2"
  local sc_value="$1"
  local sc_directive=$(echo "$sc_value" | cut -d'=' -f1 | sed 's/^[ \t]*//;s/[ \t]*$//')
  if grep -q "$sc_directive" "$2"; then
    sed -i -e "s#$sc_directive.*#$sc_value#" "$sc_config"
  else
    echo "$1" >>"$2"
  fi
}

convert_to_bytes() {
  local number=$1
  case ${number: -1} in
    "K") echo $(( ${number:0:-1} * 1024 )) ;;
    "M") echo $(( ${number:0:-1} * 1024 * 1024 )) ;;
    "G") echo $(( ${number:0:-1} * 1024 * 1024 * 1024 )) ;;
    *) echo $(( ${number} * 1 )) ;;
  esac
}

check_size_needs_update() {
  local key="$1"
  local value="$2"
  local file="$3"

  local curr_value=$(grep "${key}" "${file}" 2> /dev/null | cut -d'=' -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
  if [ -n "$curr_value" ]; then
    if [ $(convert_to_bytes ${value}) -gt $(convert_to_bytes ${curr_value}) ] ; then
      return 0
    else
      return 1
    fi
  fi

  return 0
}

#
# Setup default PHP version
#
if [ -f "/usr/bin/php${DEFAULT_PHP}" ] ; then
  run "update-alternatives --set php /usr/bin/php${DEFAULT_PHP}" "Set PHP ${DEFAULT_PHP} as default php"
  run "update-alternatives --set phar /usr/bin/phar${DEFAULT_PHP}" "Set PHP ${DEFAULT_PHP} as default phar"
  run "update-alternatives --set phar.phar /usr/bin/phar.phar${DEFAULT_PHP}" "Set PHP ${DEFAULT_PHP} as default phar.phar"
  run_file_exists "update-alternatives --set php-fpm.sock /run/php/php${DEFAULT_PHP}-fpm.sock" "Set PHP ${DEFAULT_PHP} as default php-fpm.sock" /etc/alternatives/php-fpm.sock
fi
. /etc/os-release

os_type=$ID

case "$os_type" in
"debian")
  PATH_MYSQLSERVER_CNF=/etc/mysql/mariadb.conf.d/50-server.cnf
  ;;
"ubuntu")
  PATH_MYSQLSERVER_CNF=/etc/mysql/mysql.conf.d/mysqld.cnf
  ;;
esac

PATH_MYSQLDUMP_CNF=/etc/mysql/conf.d/mysqldump.cnf
PATH_MYSQL_CNF=/etc/mysql/conf.d/mysql.cnf

if [ -f ${PATH_MYSQLSERVER_CNF} ]; then
  if check_size_needs_update max_allowed_packet ${MIN_VALUE_FOR_MYSQL_MAX_ALLOWED_PACKET_SIZE} ${PATH_MYSQLSERVER_CNF} ; then
    run "configvalue max_allowed_packet=${MIN_VALUE_FOR_MYSQL_MAX_ALLOWED_PACKET_SIZE} ${PATH_MYSQLSERVER_CNF}" "MySQL: Updating max_allowed_packet for Mysql Server"
    run "systemctl restart ${MYSQL_SERVICE}" "Restarting MySQL"
  fi
else
  log_warning "Unable to update max_allowed_packet for Mysql Server, file ${PATH_MYSQLSERVER_CNF} does not exist."
fi

if [ -f ${PATH_MYSQLDUMP_CNF} ]; then
  if check_size_needs_update max_allowed_packet ${MIN_VALUE_FOR_MYSQL_MAX_ALLOWED_PACKET_SIZE} ${PATH_MYSQLDUMP_CNF} ; then
    run "configvalue max_allowed_packet=${MIN_VALUE_FOR_MYSQL_MAX_ALLOWED_PACKET_SIZE} ${PATH_MYSQLDUMP_CNF}" "MySQL: Updating max_allowed_packet for mysqldump"
  fi
else
  log_warning "Unable to update max_allowed_packet for mysqldump, file ${PATH_MYSQLDUMP_CNF} does not exist."
fi

if [ -f ${PATH_MYSQL_CNF} ]; then
  if check_size_needs_update max_allowed_packet ${MIN_VALUE_FOR_MYSQL_MAX_ALLOWED_PACKET_SIZE} ${PATH_MYSQL_CNF} ; then
    run "configvalue max_allowed_packet=${MIN_VALUE_FOR_MYSQL_MAX_ALLOWED_PACKET_SIZE} ${PATH_MYSQL_CNF}" "MySQL: Updating max_allowed_packet for Mysql cli"
  fi
else
  log_warning "Unable to update max_allowed_packet for Mysql cli, file ${PATH_MYSQL_CNF} does not exist."
fi
