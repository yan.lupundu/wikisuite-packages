The Elasticsearch package in WikiSuite is deprecated because Elasticsearch is no longer Open Source.

Tiki still supports Elasticsearch, and you just need to install it according to your requirements.

It is replaced by Manticore Search:
https://gitlab.com/wikisuite/wikisuite-packages/-/tree/main/wikisuite-manticore
